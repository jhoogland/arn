import * as React from "react"
import { Text, View } from "react-native"
import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"

import HomeScreen from "_components/screens/HomeScreen"
import SettingsScreen from "_components/screens/SettingsScreen"
import { navThemeLight } from "_themes/default.js"

const Tab = createBottomTabNavigator()

export default function App() {
  return (
    <NavigationContainer theme={navThemeLight}>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

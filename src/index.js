import React from "react"
import registerRootComponent from "expo/build/launch/registerRootComponent"

import Navigator from "_navigations"

const App = () => <Navigator />

registerRootComponent(App)

**ARn** (Atomic React Native) is a React starter kit based on the [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) methodology.

<p class="callout info">This starter kit is completely inspired by [Diego Haz](https://github.com/diegohaz)'s [ARc](https://github.com/diegohaz/arc) (Atomic React) starter kit. I also consulted Alexander Vishnyakov's [overview of arc](https://blog.maddevs.io/best-architecture-for-the-react-project-149b377b379d), and
I took inspiration from Helder Burato Berto's post, [An efficient way to structure React Native Projects](https://cheesecakelabs.com/blog/efficient-way-structure-react-native-projects/).</p>

## Branches

- ### [`master`](https://gitlab.com/jhoogland/arn)

  The basic stack with [React](https://facebook.github.io/react/), [Expo (managed workflow)](https://docs.expo.io/), [react-navigation](https://reactnavigation.org/) and [Jest](https://facebook.github.io/jest/).

  - ### [`redux`](https://gitlab.com/jhoogland/arn/tree/redux) <sup><sub>([compare](https://gitlab.com/jhoogland/arn/compare/master...redux?diff=split#files_bucket))</sub></sup>

    Master plus [redux](https://github.com/reactjs/redux), [redux-saga](https://github.com/yelouafi/redux-saga) and [redux-form](https://github.com/erikras/redux-form).

## Why

Quoting entirely from [ARc](https://github.com/diegohaz/arc):

> React encourages you to create very small and pure components. However, as your project grows, you will have an increasingly complex components folder. At some point, this will be really huge and hard to maintain.

> I had a React project with more than 100 components in the `components` folder. The first approach I tried to organize it was separating the components by domain (described [here](http://marmelab.com/blog/2015/12/17/react-directory-structure.html)), but I realized that most of my components didn't belong to any domain, but were shared. This meant that my problems just moved to the `commons` folder.

> The [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) approach comes handy to solve this problem because it considers the reusability through composition, *which is actually what React is*. You will have your minimal/stylish components in one folder, pages in another and so on.

## Setup

### 1. Get the source code

Just clone one of the ARn [branches](#branches):
```sh
$ git clone -b master https://gitlab.com/jhoogland/arn my-app
$ cd my-app
```

You will probably want to remove ARc git history and start a brand new repository:
```sh
$ rm -rf .git
$ git init
```

### 2. Install dependencies

```sh
$ expo install
```

### 3. Run the app

```sh
$ expo run
```


## Contributing

When submitting an issue, use the following patterns in the title for better understanding:
```bash
[v0.3.1-redux] Something wrong is not right # the v0.3.1 release of the redux branch
[redux] Something wrong is not right # the actual code of the redux branch
Something wrong is not right # general, related to master or not directly related to any branch
```

PRs are very appreciated. For bugs/features consider creating an issue before sending a PR.

## License

MIT © [Jesse Hoogland](https://gitlab.com/jhoogland)

module.exports = {
  root: true,
  extends: "@react-native-community",
  plugins: ["import"],
  settings: {
    "import/resolver": {
      node: {
        paths: ["src"],
        alias: {
          _components: "./src/components",
          _containers: "./src/containers",
          _navigations: "./src/navigations",
          _services: "./src/services",
          _themes: "./src/components/themes",
          _utils: "./src/utils",
        },
      },
    },
  },
}
